<?php

/**
 * @file
 * openxvideoads.admin.inc
 */
/**
 * Form with overview of all openx video advertisement zone.
 */
function videoads_settings_form() {
  $form = array();
  $form['adserver'] = array(
    '#type' => 'fieldset',
    '#title' => t('Adserver configurations'),
    '#collapsible' => TRUE,
  );
  $form['adserver']['openxvideoads_openxurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Openx Server'),
    '#required' => TRUE,
    '#description' => t('url of the Openx Server.'),
    '#default_value' => variable_get('openxvideoads_openxurl', 'http://www.alamosa.tv/openx-2.8.2-rc14'),
  );
  $form['adserver']['openxvideoads_rtmpurl'] = array(
    '#type' => 'textfield',
    '#title' => t('RTMP Server'),
    '#required' => TRUE,
    '#description' => t('url of the RTMP Server.'),
    '#default_value' => variable_get('openxvideoads_rtmpurl', 'www.alamosa.tv:1935'),
  );
  return system_settings_form($form);
}
function videoads_zones_form() {
  $rows = array();
  $zones = openxvideoads_get_zone(NULL);
  foreach ($zones as $zone) {
    $rows[] = array(
      'name' => check_plain($zone->title),
      'edit' => l(t('Edit'), "admin/settings/openxad/edit/$zone->id"),
      'delete' => l(t('Delete'), "admin/settings/openxad/delete/$zone->id"),
    );
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No campaign available yet.'), 'colspan' => '4'));
  }
  $header = array(t('Name'), array('data' => t('Operation'), 'colspan' => '3'));
  $output = theme('table', $header, $rows, array('id' => 'zone'));
  $form = array();
  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Aviilable zone'),
   '#collapsible' => TRUE,
  );
  $form['list']['table'] = array(
    '#prefix' => '<div>',
    '#value' => $output,
    '#suffix' => '</div>',
  );

  // add zone.
  $form['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Zone.'),
  );

  $form['block']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Zone name'),
    '#description' => t('A zone with this same name will be created.'),
    '#default_value' => '',
    '#required' => TRUE,
  );
  $form['block']['zid'] = array(
    '#type' => 'textfield',
    '#title' => t('Zone Id'),
    '#description' => t('Id of the zone.'),
    '#default_value' => '',
    '#required' => TRUE,
  );

  $form['block']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Add zone'),
  );

  return $form;
}
/**
 * Validate "videoads" form.
 */
function videoads_zones_form_validate($form, &$form_state) {
  $zones = openxvideoads_get_zone(NULL);
  $zone_titles = array();
  foreach ($zones as $zone) {
    $zone_titles[] = $zone->title;
  }

  if (!empty($zone_titles)) {
    // Check if name is unique
    if (in_array($form_state['values']['title'], $zone_titles)) {
      form_set_error('', t('zone %s already exists. Please use a different name.', array('%s' => $form_state['values']['title'])));
    }
  }
}
/**
 * Add campige to database from "videoads setting form" form.
 */
function videoads_zones_form_submit($form, &$form_state) {
  db_query("INSERT INTO {openxvideoads_data} (title,zid) VALUES ('%s', %d)", $form_state['values']['title'], $form_state['values']['zid']);
  drupal_set_message(t('Zone %s is added.', array('%s' => $form_state['values']['title'])));
}
function videoads_defaultads_form() {
  $rows = array();
  $zones = openxvideoads_get_zone(NULL);
  $zoneoptions = array();
  foreach ($zones as $zone) {
    $zoneoptions[] = $zone->title;
  }
  $form = array();
  $form['enabledefaultad'] = array (
    '#type' => 'checkbox',
    '#title' => 'Enable default advertisement',
    '#default_value' => variable_get('openxvideoads_enabledefaultfad', FALSE),
  );
  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default advertisements'),
    '#collapsible' => TRUE,
  );
  $form['list']['#theme'] = 'openxvideoads_form_current';
  $form['list']['#tree'] = TRUE;
  $query = db_query("SELECT * FROM {openxvideoads_defaultads}");
  while($object = db_fetch_object($query)) {
    $form['list'][$object->id]['remove'] = array('#type' => 'checkbox', '#default_value' => FALSE);
    $form['list'][$object->id]['zone'] = array('#type' => 'select', '#options' => $zoneoptions, '#value' => $object->zid);
    $form['list'][$object->id]['adposition'] = array('#type' => 'select', '#options' => array('Pre-roll', 'Mid-roll', 'Post-roll'), '#value' => $object->position);
    $form['list'][$object->id]['starttime'] = array('#type' => 'textfield', '#size' => 20,'#value' => $object->starttime);
  }
  $header = array(t('Zone'),t('Position'),t('Start Time'));
  $output = theme('table', $header, $rows, array('id' => 'zone'));
  // add zone.
  $form['block']['#weight'] = 10;
  $form['block']['#theme'] = 'openxvideoads_defaultad_form';
  $form['block']['zone'] = array(
      '#type' => 'select',
      '#title' => t('Select a zone'),
      '#options' => $zoneoptions,
      '#description' => "select a zone from the dropdown",
  );
  $form['block']['adposition'] = array(
       '#type' => 'select',
       '#title' => t('Position'),
       '#options' => array('Pre-roll', 'Mid-roll', 'Post-roll'),
       '#description' => t('Select an ad position'),
  );
  $form['block']['starttime'] = array(
    '#type' => 'textfield',
    '#size' => 20,
    '#default_value' => '0',
    '#title' => t('Start Time'),
    '#description' => t('start time in % must be between 1-100'),
  );
  $form['block']['op'] = array(
    '#type' => 'submit',
    '#value' => t('Add zone'),
  );
  $form['save'] = array (
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  return $form;
}
/**
 * Validate "videoads" form.
 */
function videoads_defaultads_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['starttime']) || $form_state['values']['starttime'] < 0 || $form_state['values']['starttime'] > 100) {
    form_error($form, "Start time must be a numeric value in % between 0-100");
    return;
  }
}
/**
 * Add campige to database from "videoads setting form" form.
 */
function videoads_defaultads_form_submit($form, &$form_state) {
  if($form_state['values']['op'] == 'Add zone') {
    db_query("INSERT INTO {openxvideoads_defaultads} (zid, position, starttime) VALUES ('%d', %d, %d)", $form_state['values']['zone'], $form_state['values']['adposition'], $form_state['values']['starttime']);
    drupal_set_message(t('New default Advertisement is added'));
  }
  else {
    foreach ($form_state['values']['list'] as $id => $ads) {
     if(!empty($ads['remove'])){
        db_query("DELETE FROM {openxvideoads_defaultads} WHERE id = %d", $id);
     }
   }
   variable_set('openxvideoads_enabledefaultfad',$form_state['values']['enabledefaultad']);
  }
}

/**
 * Edit zone form.
 */
function videoads_edit_form(&$form_state, $id) {
  $zone = openxvideoads_get_zone($id);
  $form = array();

  $form['delta'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Zone Name'),
    '#description' => t('The zone name must be unique.'),
    '#default_value' => $zone->title,
    '#required' => TRUE,
  );
  $form['zid'] = array(
    '#type' => 'textfield',
    '#title' => t('Zone Id'),
    '#description' => t('The zone id must be unique.'),
    '#default_value' => $zone->zid,
    '#required' => TRUE,
  );
  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['#redirect'] = 'admin/settings/openxad';
  return $form;
}

/**
 * Validate edit zone form.
 */
function videoads_edit_form_validate($form, &$form_state) {
  $zones = openxvideoads_get_zone(NULL);
  $zone_titles = array();
  foreach ($zones as $zone) {
    $zone_titles[] = $zone->title;
  }

  // Remove current blockname to prevent false error.
  unset($zone_titles[array_search($form_state['values']['title'], $zone_titles)]);

  if (!empty( $zone_titles)) {
    // Check if name is unique.
    if (in_array($form_state['values']['title'], $zone_titles)) {
      form_set_error('', t('Zone %s already exists. Please use a different name.', array('%s' => $form_state['values']['title'])));
    }
  }
}

/**
 * Submit edit zone form.
 */
function videoads_edit_form_submit($form, &$form_state) {
  db_query("UPDATE {openxvideoads_data} SET title = '%s', zid = %d WHERE id = %d",
  $form_state['values']['title'], $form_state['values']['zid'], $form_state['values']['delta']);
  drupal_set_message(t('Zone  updated.'));
  return 'admin/settings/openxad';
}
/**
 * Delete zone form.
 */
function videoads_confirm_delete_form(&$form_state, $id) {
  $zone = openxvideoads_get_zone($id);
  if (empty($zone)) {
    drupal_set_message(t('The Zone with delta @delta was not found.', array('@delta' => $id)), 'error');
   // return array();
  }
  else {
    //The question to ask the user.
    $question = t('Are you sure you want to delete the Zone %title?', array('%title' => $zone->title));
    // The page to go to if the user denies the action.
    $path = 'admin/settings/openxad';
    // Additional text to display (defaults to "This action cannot be undone.").
    $description = t('This action cannot be undone.');
    //  A caption for the button which confirms the action.
    $yes = t('Delete');
    // A caption for the link which denies the action.
    $no = t('Cancel');
    // set delta value to use in submit function.
    $form['delta'] = array('#type' => 'value', '#value' => $id);
    // set the redirect path value to use in submit function.
    $form['#redirect'] = $path;
    return confirm_form(
      $form,
      $question,
      $path,
      $description,
      $yes,
      $no
    );
  }
}

/**
 * Delete a zone.
 */
function videoads_confirm_delete_form_submit($form, &$form_state) {
  if (videoads_confirm_delete($form_state['values']['delta'])) {
    drupal_set_message(t('Zone successfully deleted!'));
  }
  else {
    drupal_set_message(t('There was a problem deleting the Zone instance'));
  }
}

/**
 * Delete zone.
 */
function videoads_confirm_delete($id) {
  $result = db_query('DELETE FROM {openxvideoads_data} WHERE id = %d', (int)$id);
  return TRUE;
}
function theme_openxvideoads_defaultad_form(&$form) {
  //$header = array('',  t('Zone'), t('Ad Position'), t('Start Time'));
  $row = array('');
  $row[] = drupal_render($form['zone']);
  $row[] = drupal_render($form['adposition']);
  $row[] = drupal_render($form['starttime']);
  $row[] = drupal_render($form['op']);
  $rows[] = array('data' => $row, 'class' => 'draggable');
  $output = theme('table', null, $rows, array('id' => 'upload-openxvideoads'));
  $output .= drupal_render($form);
 
  return $output;
}

